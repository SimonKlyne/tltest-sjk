#!/bin/bash

# Pack up contents of directory into archive into self unpacking archive
#
# Usage: pack.sh dir archive

sourceDir=$1
archive=$2
[[ -z "$archive" ]] && archive="default-arc.sh"

[[ -d $sourceDir ]] && ( cd $sourceDir && tar cf - . | uuencode - > /tmp/source.tar.uu )

# Create shell archive script

cat > $archive <<EOARCH
#!/bin/bash
set -x
targetDir=\$1
[[ -d \$targetDir ]] || mkdir \$targetDir
uudecode << 'EOF' | ( cd \$targetDir && tar xf -)
EOARCH
# Now dump the contents of the UUencoded archive in the self unpacking archive script
cat /tmp/source.tar.uu >> $archive
# Write the end
cat >> $archive <<EOARCTAIL
EOF
find \$targetDir
EOARCTAIL
chmod 755 $archive
