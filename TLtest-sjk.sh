#!/bin/bash


resultsDir=$HOME/TrueLayer
resultsF=$resultsDir/TLTest.txt

if [ ! -d $resultsDir ]; then
    mkdir $resultsDir
fi

touch $resultsF
echo "Run time $(date +%H:%M)" 		>> $resultsF
echo "Script name : $(basename $0)" 	>> $resultsF
echo "Contents of current directory:"	>> $resultsF
ls -a 					>> $resultsF
echo "Public IPaddr: $(curl 'https://api.ipify.org?format=txt')"	>> $resultsF
memk=$(grep MemTotal /proc/meminfo | awk '{print $2}')
memM=$(( memk / 1024 ))
echo "Total Memory $memM Mb" 				>> $resultsF
echo "User executing $0 : $(whoami)"			>> $resultsF
echo "PID of current process : $$"			>> $resultsF
echo 							>> $resultsF



